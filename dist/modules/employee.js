'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Employee = {};

/**
 * Create a manager
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - uinty identifier
 * @param {String} name - manager name
 * @param {String} email - manager email
 * @returns {Promise}
 */
Employee.createManager = async function (uuidClient, uuidUnity, name, email) {
  var data = {
    email: email,
    name: name,
    uuid_client: uuidClient,
    uuid_unity: uuidUnity
  };
  var response = await _config2.default.post('createmanager', data);
  return response.data;
};

/**
 * Create a operator
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - uinty identifier
 * @param {String} name - operator name
 * @param {String} email - operator email
 * @param {Boolean} activateCircuits - confirm if operator can activate circuits
 * @returns
 */
Employee.createOperator = async function (uuidClient, uuidUnity, name, email, activateCircuits) {
  var data = {
    email: email,
    name: name,
    uuid_client: uuidClient,
    uuid_unity: uuidUnity,
    activate_circuits: activateCircuits
  };
  var response = await _config2.default.post('createoperator', data);
  return response.data;
};

/**
 * Get all employees of a client'unity
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - unity identifier
 * @param {Object} pageCode - (optional) - code of the page - default : '0'
 * @param {Number} pageSize - (optional) - size of the page - default : 300
 * @returns {Promise}
 */
Employee.getAllEmployee = async function (uuidClient, uuidUnity) {
  var pageCode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '0';
  var pageSize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 300;

  var params = {
    uuid_unity: uuidUnity,
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallemployee', { params: params });
  return response.data;
};

/**
 * Get an employee
 * @param {String} uuidEmployee - employee identifier
 * @returns {Promise}
 */
Employee.getEmployee = async function (uuidEmployee) {
  var params = {
    uuid_employee: uuidEmployee
  };

  var response = await _config2.default.get('getemployee', { params: params });
  return response.data;
};

/**
 * delete an employee
 * @param {String} uuidEmployee - employee idnetifier
 * @returns {Promise}
 */
Employee.deleteEmployee = async function (uuidEmployee) {
  var params = {
    uuid_employee: uuidEmployee
  };

  var response = await _config2.default.delete('deleteemployee', { data: params });
  return response.data;
};

/**
 * Update a employee
 * @param {String} uuidEmployee - employee identifier
 * @param {String} name - employee new name
 * @param {String} email - employee new e-mail
 * @returns {Promise}
 */
Employee.updateEmployee = async function (_ref) {
  var uuidEmployee = _ref.uuidEmployee,
      name = _ref.name,
      cep = _ref.cep,
      cpf = _ref.cpf,
      rua = _ref.rua,
      numero = _ref.numero,
      bairro = _ref.bairro,
      cidade = _ref.cidade,
      estado = _ref.estado,
      complemento = _ref.complemento,
      moradores = _ref.moradores;

  var data = {
    uuid_employee: uuidEmployee,
    name: name,
    cep: cep,
    cpf: cpf,
    rua: rua,
    numero: numero,
    bairro: bairro,
    cidade: cidade,
    estado: estado,
    complemento: complemento,
    moradores: moradores
  };

  var response = await _config2.default.put('updateemployee', data);
  return response.data;
};

exports.default = Employee;