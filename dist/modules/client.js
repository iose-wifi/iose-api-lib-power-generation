'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Client = {};

/**
 * Create a client
 * @param {String} name - company name
 * @param {String} description - company  description
 * @return {Promise}
 */
Client.create = async function (name, description) {
  var data = {
    company_name: name,
    company_description: description
  };
  var response = await _config2.default.post('createclient', data);
  return response.data;
};

/**
 * Get data of a client
 * @param {String} uuidClient - client identifier
 * @returns {Promise}
 */
Client.get = async function (uuidClient) {
  var params = {
    uuid_client: uuidClient
  };
  var response = await _config2.default.get('getclient', { params: params });
  return response.data;
};

/**
 * Get all clients
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @returns {Promise}
 */
Client.getAll = async function () {
  var pageCode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '0';
  var pageSize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 300;

  var params = {
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallclient', { params: params });
  return response.data;
};

/**
 * Delete a client
 * @param {String} uuidClient - client identifier
 * @returns {Promise}
 */
Client.delete = async function (uuidClient) {
  var params = {
    uuid_client: uuidClient
  };

  var response = await _config2.default.delete('deleteclient', { data: params });
  return response.data;
};

/**
 * Update a client
 * @param {String} uuidClient - client identifier
 * @param {String} name -client name
 * @param {String} description -client description
 * @returns {Promise}
 */
Client.update = async function (uuidClient, name, description) {
  var params = {
    uuid_client: uuidClient,
    name: name,
    description: description
  };
  var response = await _config2.default.put('updateclient', params);
  return response.data;
};

exports.default = Client;