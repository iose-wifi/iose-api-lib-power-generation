'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Notifications = {};

/**
 * Create a Device Notification
 * @param {String} device_token - device identifier
 * @param {String} uuid_unity - unity identifier
 * @param {String} device_type - OS device (Android or IOS)
 * @return {Promise}
 */
Notifications.createDeviceNotification = async function (_ref) {
  var device_token = _ref.device_token,
      uuid_unity = _ref.uuid_unity,
      device_type = _ref.device_type;

  var data = {
    device_token: device_token,
    uuid_unity: uuid_unity,
    device_type: device_type
  };

  var response = await _config2.default.post('createdevicenotification', data);
  return response.data;
};

/**
 * Delete a Device Notification
 * @param {String} device_token - device identifier
 * @returns {Promise}
 */
Notifications.deleteDeviceNotification = async function (device_token) {
  var params = {
    device_token: device_token
  };

  var response = await _config2.default.delete('deletedevicenotification', {
    data: params
  });
  return response.data;
};

exports.default = Notifications;