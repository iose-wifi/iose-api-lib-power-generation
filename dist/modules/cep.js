'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Cep = {};

/**
 * Get data of an Cep
 * @param {String} numberCep -Cep number
 * @returns {Promise}
 */

Cep.getCep = async function (numberCep) {
  var axiosNewBaseURL = _config2.default.create({
    baseURL: 'https://viacep.com.br/',
    timeout: 30000
  });

  var response = await axiosNewBaseURL.get('ws/' + numberCep + '/json');

  return response;
};
exports.default = Cep;