'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Logs = {};

/**
 * Get all logs circuit
 * @param {String} uuid_circuit - client identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @return {Promise}
 */
Logs.getAllLogs = async function (uuid_circuit) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_circuit: uuid_circuit,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getalllogs', { params: params });
  return response.data;
};

exports.default = Logs;