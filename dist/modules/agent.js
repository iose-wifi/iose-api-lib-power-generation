'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Agent = {};

/**
 * Get all agents existents
 * @returns {Promise}
 */
Agent.getAllAgents = async function () {
  var response = await _config2.default.get('getallagents');
  return response.data;
};

exports.default = Agent;