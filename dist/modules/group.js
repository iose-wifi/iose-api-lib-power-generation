'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Group = {};

/**
 * Create a Group
 * @param {String} uuidUnity - unity identifier
 * @param {String} name - group name
 * @param {String} description - group description
 * @param {Number} goal - goal value
 * @returns {Promise}
 */
Group.createGroup = async function (uuidUnity, name, description, goal) {
  var params = {
    uuid_unity: uuidUnity,
    name: name,
    description: description,
    goal: goal
  };
  var response = await _config2.default.post('creategroup', params);
  return response.data;
};

/**
 * Delete a group
 * @param {String} uuidGroup - group identifier
 * @returns {Promise}
 */
Group.deleteGroup = async function (uuidGroup) {
  var params = {
    uuid_group: uuidGroup
  };
  var response = await _config2.default.delete('deletegroup', { data: params });
  return response.data;
};

/**
 * Update a group
 * @param {String} uuidGroup - group identifier
 * @param {String} name - group new name
 * @param {String} description - group new description
 * @param {Number} goal -group new goal
 * @returns {Promise}
 */
Group.updateGroup = async function (uuidGroup, name, description, goal) {
  var params = {
    uuid_group: uuidGroup,
    name: name,
    description: description,
    goal: goal
  };
  var response = await _config2.default.put('updategroup', params);
  return response.data;
};

/**
 * Get all groups of an unity
 * @param {String} uuidUnity - unity identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {Number} pageSize - (optional) size of the page - default: 300
 * @returns
 */
Group.getAllGroup = async function (uuidUnity) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_unity: uuidUnity,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallgroup', { params: params });
  return response.data;
};

exports.default = Group;