'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Admin = {};

/**
 * Create a admin
 * @param {String} uuid_client - client identifier
 * @param {String} name - administrator name
 * @param {String} email - administrator  email
 * @return {Promise}
 */
Admin.createAdmin = async function (uuid_client, name, email) {
  var data = {
    name: name,
    email: email,
    uuid_client: uuid_client
  };
  var response = await _config2.default.post('createadmin', data);
  return response.data;
};

/**
 * Get data of an admin
 * @param {String} uuidAdmin -admin identifier
 * @returns {Promise}
 */
Admin.getAdmin = async function (uuidAdmin) {
  var params = {
    uuid_admin: uuidAdmin
  };
  var response = await _config2.default.get('getadmin', { params: params });
  return response.data;
};

/**
 * Get all admins of a client
 * @param {String} uuidClient - client identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @returns {Promise}
 */
Admin.getAllAdmin = async function (uuidClient) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getalladmin', { params: params });
  return response.data;
};

/**
 * Delete an admin
 * @param {String} uuidAdmin - admin identifier
 * @returns {Promise}
 */
Admin.deleteAdmin = async function (uuidAdmin) {
  var params = {
    uuid_admin: uuidAdmin
  };

  var response = await _config2.default.delete('deleteadmin', { data: params });
  return response.data;
};

/**
 * Update an admin
 * @param {String} uuidAdmin - admin identifier
 * @param {String} name -admin name
 * @param {String} email -admin email
 * @returns {Promise}
 */
Admin.updateAdmin = async function (_ref) {
  var uuidAdmin = _ref.uuidAdmin,
      name = _ref.name,
      cep = _ref.cep,
      cpf = _ref.cpf,
      rua = _ref.rua,
      numero = _ref.numero,
      bairro = _ref.bairro,
      cidade = _ref.cidade,
      estado = _ref.estado;

  var params = {
    uuid_admin: uuidAdmin,
    name: name,
    cep: cep,
    cpf: cpf,
    rua: rua,
    numero: numero,
    bairro: bairro,
    cidade: cidade,
    estado: estado
  };
  var response = await _config2.default.put('updateadmin', params);
  return response.data;
};

exports.default = Admin;