'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Unity = {};

/**
 * Create an unity
 * @param {String} uuidClient - client identifier
 * @param {String} name - uinty name
 * @param {String} description - unity description
 * @param {Number} goal - goal of unity
 * @param {Number} tariffPeriod - period of the tariff
 * @param {Number} contractedDemand - contracted Demand
 * @param {Object} tariffData  - data of the tariff
 * @returns {Promise}
 */
Unity.createUnity = async function (uuidClient, name, description, goal, tariffPeriod, contractedDemand, tariffData) {
  var params = {
    uuid_client: uuidClient,
    name: name,
    description: description,
    goal: goal,
    tariff_period: tariffPeriod,
    contracted_demand: contractedDemand,
    tariff_data: tariffData
  };
  var response = await _config2.default.post('createunity', params);
  return response.data;
};

/**
 * Delete an unity
 * @param {String} uuidUnity - unity identifier
 * @returns {Promise}
 */
Unity.deleteUnity = async function (uuidUnity) {
  var params = {
    uuid_unity: uuidUnity
  };

  var response = await _config2.default.delete('deleteunity', { data: params });
  return response.data;
};

/**
 * Update unity
 * @param {String} uuidUnity - unity identifier
 * @param {String} name - unity new name
 * @param {String} description - unity new description
 * @param {Number} goal - new goal of unity
 * @param {Number} tariffPeriod - new period of the tariff
 * @param {Number} contractedDemand - new contracted Demand
 * @param {Object} tariffData  - new data of the tariff

 * @returns {Promise}
 */
Unity.updateUnity = async function (_ref) {
  var uuid_unity = _ref.uuid_unity,
      name = _ref.name,
      description = _ref.description,
      goal = _ref.goal,
      tariff_period = _ref.tariff_period,
      contracted_demand = _ref.contracted_demand,
      tariff_data = _ref.tariff_data;

  var params = {
    uuid_unity: uuid_unity,
    name: name,
    description: description,
    goal: goal,
    tariff_period: tariff_period,
    contracted_demand: contracted_demand,
    tariff_data: tariff_data
  };

  var response = await _config2.default.put('updateunity', params);
  return response.data;
};

/**
 * Get a unity
 * @param {String} uuidUnity - unity identifier
 * @returns
 */
Unity.getUnity = async function (uuidUnity) {
  var params = {
    uuid_unity: uuidUnity
  };
  var response = await _config2.default.get('getunity', { params: params });
  return response.data;
};

/**
 * Get all unitys of a client
 * @param {String} uuidClient
 * @param {Object} pageCode - (optional) - code of the page - default : '0'
 * @param {Number} pageSize - (optional) - size of the page - default : 300
 * @returns
 */
Unity.getAllUnity = async function (uuidClient) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallunity', { params: params });
  return response.data;
};

exports.default = Unity;