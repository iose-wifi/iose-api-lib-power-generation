'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HouseholdAppliance = {};

/**
 * Create a Holsehold Appliance
 * @param {Object} param - object containing { name, uuidUnity, uuidGroup, uuidCircuit, quantity, hours, potency}
 * @returns {Promise}
 */
HouseholdAppliance.create = async function (_ref) {
  var name = _ref.name,
      label = _ref.label,
      uuidUnity = _ref.uuidUnity,
      uuidGroup = _ref.uuidGroup,
      uuidCircuit = _ref.uuidCircuit,
      quantity = _ref.quantity,
      hours = _ref.hours,
      potency = _ref.potency;

  var data = {
    name: name,
    label: label,
    uuid_unity: uuidUnity,
    uuid_group: uuidGroup,
    uuid_circuit: uuidCircuit,
    quantity: quantity,
    hours: hours,
    potency: potency
  };
  var response = await _config2.default.post('createhouseholdappliance', data);
  return response.data;
};

/**
 * Get data of a househol appliance by circuit id
 * @param {String} uuidAdmin - circuit identifier
 * @returns {Promise}
 */
HouseholdAppliance.getByCircuit = async function (uuidCircuit) {
  var params = {
    uuid_circuit: uuidCircuit
  };
  var response = await _config2.default.get('gethouseholdappliancebycircuit', {
    params: params
  });
  return response.data;
};

/**
 * Delete a household Appliance
 * @param {String} uuidAdmin - household appliance identifier
 * @returns {Promise}
 */
HouseholdAppliance.delete = async function (uuidHouseholdAppliances) {
  var params = {
    uuid_household_appliances: uuidHouseholdAppliances
  };

  var response = await _config2.default.delete('deletehouseholdappliance', {
    data: params
  });
  return response.data;
};

/**
 * Update a Holsehold Appliance
 * @param {Object} param - object containing {uuidHouseholdAppliances, name, quantity, hours, potency}
 * @returns {Promise}
 */
HouseholdAppliance.update = async function (_ref2) {
  var uuidHouseholdAppliances = _ref2.uuidHouseholdAppliances,
      name = _ref2.name,
      label = _ref2.label,
      quantity = _ref2.quantity,
      hours = _ref2.hours,
      potency = _ref2.potency;

  var params = {
    uuid_household_appliances: uuidHouseholdAppliances,
    name: name,
    label: label,
    quantity: quantity,
    hours: hours,
    potency: potency
  };

  var response = await _config2.default.put('updatehouseholdappliance', params);
  return response.data;
};

/**
 * Get all default data
 * @returns {Promise}
 */
HouseholdAppliance.getDefaultValues = async function () {
  var response = await _config2.default.get('getdefaulthouseholdappliance');
  return response.data;
};

exports.default = HouseholdAppliance;