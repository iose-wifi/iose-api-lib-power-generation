import axios from './config';

const Employee = {};

/**
 * Create a manager
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - uinty identifier
 * @param {String} name - manager name
 * @param {String} email - manager email
 * @returns {Promise}
 */
Employee.createManager = async (uuidClient, uuidUnity, name, email) => {
  const data = {
    email: email,
    name: name,
    uuid_client: uuidClient,
    uuid_unity: uuidUnity,
  };
  const response = await axios.post('createmanager', data);
  return response.data;
};

/**
 * Create a operator
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - uinty identifier
 * @param {String} name - operator name
 * @param {String} email - operator email
 * @param {Boolean} activateCircuits - confirm if operator can activate circuits
 * @returns
 */
Employee.createOperator = async (
  uuidClient,
  uuidUnity,
  name,
  email,
  activateCircuits,
) => {
  let data = {
    email: email,
    name: name,
    uuid_client: uuidClient,
    uuid_unity: uuidUnity,
    activate_circuits: activateCircuits,
  };
  const response = await axios.post('createoperator', data);
  return response.data;
};

/**
 * Get all employees of a client'unity
 * @param {String} uuidClient - client identifier
 * @param {String} uuidUnity - unity identifier
 * @param {Object} pageCode - (optional) - code of the page - default : '0'
 * @param {Number} pageSize - (optional) - size of the page - default : 300
 * @returns {Promise}
 */
Employee.getAllEmployee = async (
  uuidClient,
  uuidUnity,
  pageCode = '0',
  pageSize = 300,
) => {
  const params = {
    uuid_unity: uuidUnity,
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallemployee', {params: params});
  return response.data;
};

/**
 * Get an employee
 * @param {String} uuidEmployee - employee identifier
 * @returns {Promise}
 */
Employee.getEmployee = async uuidEmployee => {
  let params = {
    uuid_employee: uuidEmployee,
  };

  const response = await axios.get('getemployee', {params: params});
  return response.data;
};

/**
 * delete an employee
 * @param {String} uuidEmployee - employee idnetifier
 * @returns {Promise}
 */
Employee.deleteEmployee = async uuidEmployee => {
  const params = {
    uuid_employee: uuidEmployee,
  };

  const response = await axios.delete('deleteemployee', {data: params});
  return response.data;
};

/**
 * Update a employee
 * @param {String} uuidEmployee - employee identifier
 * @param {String} name - employee new name
 * @param {String} email - employee new e-mail
 * @returns {Promise}
 */
Employee.updateEmployee = async ({
  uuidEmployee,
  name,
  cep,
  cpf,
  rua,
  numero,
  bairro,
  cidade,
  estado,
  complemento,
  moradores,
}) => {
  const data = {
    uuid_employee: uuidEmployee,
    name: name,
    cep: cep,
    cpf: cpf,
    rua: rua,
    numero: numero,
    bairro: bairro,
    cidade: cidade,
    estado: estado,
    complemento: complemento,
    moradores: moradores,
  };

  const response = await axios.put('updateemployee', data);
  return response.data;
};

export default Employee;
