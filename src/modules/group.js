import axios from './config';

const Group = {};

/**
 * Create a Group
 * @param {String} uuidUnity - unity identifier
 * @param {String} name - group name
 * @param {String} description - group description
 * @param {Number} goal - goal value
 * @returns {Promise}
 */
Group.createGroup = async (uuidUnity, name, description, goal) => {
  const params = {
    uuid_unity: uuidUnity,
    name: name,
    description: description,
    goal: goal,
  };
  const response = await axios.post('creategroup', params);
  return response.data;
};

/**
 * Delete a group
 * @param {String} uuidGroup - group identifier
 * @returns {Promise}
 */
Group.deleteGroup = async uuidGroup => {
  const params = {
    uuid_group: uuidGroup,
  };
  const response = await axios.delete('deletegroup', {data: params});
  return response.data;
};

/**
 * Update a group
 * @param {String} uuidGroup - group identifier
 * @param {String} name - group new name
 * @param {String} description - group new description
 * @param {Number} goal -group new goal
 * @returns {Promise}
 */
Group.updateGroup = async (uuidGroup, name, description, goal) => {
  const params = {
    uuid_group: uuidGroup,
    name: name,
    description: description,
    goal: goal,
  };
  const response = await axios.put('updategroup', params);
  return response.data;
};

/**
 * Get all groups of an unity
 * @param {String} uuidUnity - unity identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {Number} pageSize - (optional) size of the page - default: 300
 * @returns
 */
Group.getAllGroup = async (uuidUnity, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_unity: uuidUnity,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallgroup', {params: params});
  return response.data;
};

export default Group;
