import axios from './config';

const Flag = {};

/**
 * Get the most recent flag
 * @returns {Promise}
 */
Flag.getLastFlag = async () => {
  const response = await axios.get('getlastflag');
  return response.data;
};

export default Flag;
