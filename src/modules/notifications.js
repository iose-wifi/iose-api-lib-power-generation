import axios from './config';

const Notifications = {};

/**
 * Create a Device Notification
 * @param {String} device_token - device identifier
 * @param {String} uuid_unity - unity identifier
 * @param {String} device_type - OS device (Android or IOS)
 * @return {Promise}
 */
Notifications.createDeviceNotification = async ({
  device_token,
  uuid_unity,
  device_type,
}) => {
  const data = {
    device_token: device_token,
    uuid_unity: uuid_unity,
    device_type: device_type,
  };

  const response = await axios.post('createdevicenotification', data);
  return response.data;
};

/**
 * Delete a Device Notification
 * @param {String} device_token - device identifier
 * @returns {Promise}
 */
Notifications.deleteDeviceNotification = async device_token => {
  const params = {
    device_token: device_token,
  };

  const response = await axios.delete('deletedevicenotification', {
    data: params,
  });
  return response.data;
};

export default Notifications;
