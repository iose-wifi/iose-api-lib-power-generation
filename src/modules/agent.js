import axios from './config';

const Agent = {};

/**
 * Get all agents existents
 * @returns {Promise}
 */
Agent.getAllAgents = async () => {
  const response = await axios.get('getallagents');
  return response.data;
};

export default Agent;