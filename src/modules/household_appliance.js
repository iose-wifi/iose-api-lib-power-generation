import axios from './config';

const HouseholdAppliance = {};

/**
 * Create a Holsehold Appliance
 * @param {Object} param - object containing { name, uuidUnity, uuidGroup, uuidCircuit, quantity, hours, potency}
 * @returns {Promise}
 */
HouseholdAppliance.create = async ({
  name,
  label,
  uuidUnity,
  uuidGroup,
  uuidCircuit,
  quantity,
  hours,
  potency,
}) => {
  const data = {
    name: name,
    label: label,
    uuid_unity: uuidUnity,
    uuid_group: uuidGroup,
    uuid_circuit: uuidCircuit,
    quantity: quantity,
    hours: hours,
    potency: potency,
  };
  const response = await axios.post('createhouseholdappliance', data);
  return response.data;
};

/**
 * Get data of a househol appliance by circuit id
 * @param {String} uuidAdmin - circuit identifier
 * @returns {Promise}
 */
HouseholdAppliance.getByCircuit = async uuidCircuit => {
  const params = {
    uuid_circuit: uuidCircuit,
  };
  const response = await axios.get('gethouseholdappliancebycircuit', {
    params: params,
  });
  return response.data;
};

/**
 * Delete a household Appliance
 * @param {String} uuidAdmin - household appliance identifier
 * @returns {Promise}
 */
HouseholdAppliance.delete = async uuidHouseholdAppliances => {
  const params = {
    uuid_household_appliances: uuidHouseholdAppliances,
  };

  const response = await axios.delete('deletehouseholdappliance', {
    data: params,
  });
  return response.data;
};

/**
 * Update a Holsehold Appliance
 * @param {Object} param - object containing {uuidHouseholdAppliances, name, quantity, hours, potency}
 * @returns {Promise}
 */
HouseholdAppliance.update = async ({
  uuidHouseholdAppliances,
  name,
  label,
  quantity,
  hours,
  potency,
}) => {
  let params = {
    uuid_household_appliances: uuidHouseholdAppliances,
    name: name,
    label: label,
    quantity: quantity,
    hours: hours,
    potency: potency,
  };

  const response = await axios.put('updatehouseholdappliance', params);
  return response.data;
};

/**
 * Get all default data
 * @returns {Promise}
 */
HouseholdAppliance.getDefaultValues = async () => {
  const response = await axios.get('getdefaulthouseholdappliance');
  return response.data;
};

export default HouseholdAppliance;
