import axios from './config';

const Cep = {};

/**
 * Get data of an Cep
 * @param {String} numberCep -Cep number
 * @returns {Promise}
 */

Cep.getCep = async numberCep => {
  const axiosNewBaseURL = axios.create({
    baseURL: 'https://viacep.com.br/',
    timeout: 30000,
  });

  const response = await axiosNewBaseURL.get(`ws/${numberCep}/json`);

  return response;
};
export default Cep;