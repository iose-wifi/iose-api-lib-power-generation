import axios from './config';

const Unity = {};

/**
 * Create an unity
 * @param {String} uuidClient - client identifier
 * @param {String} name - uinty name
 * @param {String} description - unity description
 * @param {Number} goal - goal of unity
 * @param {Number} tariffPeriod - period of the tariff
 * @param {Number} contractedDemand - contracted Demand
 * @param {Object} tariffData  - data of the tariff
 * @returns {Promise}
 */
Unity.createUnity = async (
  uuidClient,
  name,
  description,
  goal,
  tariffPeriod,
  contractedDemand,
  tariffData,
) => {
  const params = {
    uuid_client: uuidClient,
    name: name,
    description: description,
    goal: goal,
    tariff_period: tariffPeriod,
    contracted_demand: contractedDemand,
    tariff_data: tariffData,
  };
  const response = await axios.post('createunity', params);
  return response.data;
};

/**
 * Delete an unity
 * @param {String} uuidUnity - unity identifier
 * @returns {Promise}
 */
Unity.deleteUnity = async uuidUnity => {
  const params = {
    uuid_unity: uuidUnity,
  };

  const response = await axios.delete('deleteunity', {data: params});
  return response.data;
};

/**
 * Update unity
 * @param {String} uuidUnity - unity identifier
 * @param {String} name - unity new name
 * @param {String} description - unity new description
 * @param {Number} goal - new goal of unity
 * @param {Number} tariffPeriod - new period of the tariff
 * @param {Number} contractedDemand - new contracted Demand
 * @param {Object} tariffData  - new data of the tariff

 * @returns {Promise}
 */
Unity.updateUnity = async ({
  uuid_unity,
  name,
  description,
  goal,
  tariff_period,
  contracted_demand,
  tariff_data,
}) => {
  const params = {
    uuid_unity: uuid_unity,
    name: name,
    description: description,
    goal: goal,
    tariff_period: tariff_period,
    contracted_demand: contracted_demand,
    tariff_data: tariff_data,
  };

  const response = await axios.put('updateunity', params);
  return response.data;
};

/**
 * Get a unity
 * @param {String} uuidUnity - unity identifier
 * @returns
 */
Unity.getUnity = async uuidUnity => {
  const params = {
    uuid_unity: uuidUnity,
  };
  const response = await axios.get('getunity', {params: params});
  return response.data;
};

/**
 * Get all unitys of a client
 * @param {String} uuidClient
 * @param {Object} pageCode - (optional) - code of the page - default : '0'
 * @param {Number} pageSize - (optional) - size of the page - default : 300
 * @returns
 */
Unity.getAllUnity = async (uuidClient, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallunity', {params: params});
  return response.data;
};

export default Unity;
